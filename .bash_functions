#file management
cp() { rsync -ahr --no-compress --info=progress2 --inplace "$1" "$2"; }

mdcd() { mkdir -vp "$@" && cd "$@"; }

n() {
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # To cd on quit only on ^G, either remove the "export" as in:
    #    NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    #    (or, to a custom path: NNN_TMPFILE=/tmp/.lastd)
    # or, export NNN_TMPFILE after nnn invocation
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
		. "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

# network
funvpn() { sudo openvpn --config ~/private/funvpn_nook.ovpn; }
myvpn() { sudo openvpn --config ~/private/ccnet_nook.ovpn; }
sshc() {
	echo "The SSH Connector has the following SSH-connections available:"
	export PS3="Please choose one: "
	hosts="$(grep Host ~/.ssh/config | sed '/^#/d' | grep -iv HostName | grep -iv bitbucket | grep -iv github | awk '{print $2}')"
	select host in ${hosts}; do echo "Connecting to ${host}..."; echo ""; ssh ${host}; break; done
}

# misc
colorblocks() {
	f=3 b=4
	for j in f b; do
		for i in {0..7}; do
			printf -v $j$i %b "\e[${!j}${i}m"
		done
	done

	d=$'\e[1m'
	t=$'\e[0m'
	v=$'\e[7m'

	cat << EOF
 
 $f1██████$d██$t $f2██████$d██$t $f3██████$d██$t $f4██████$d██$t $f5██████$d██$t $f6██████$d██$t 
 $f1██████$d██$t $f2██████$d██$t $f3██████$d██$t $f4██████$d██$t $f5██████$d██$t $f6██████$d██$t 
 $f1██████$d██$t $f2██████$d██$t $f3██████$d██$t $f4██████$d██$t $f5██████$d██$t $f6██████$d██$t 
 $ft██████$d$f7██$t $ft██████$d$f7██$t $ft██████$d$f7██$t $ft██████$d$f7██$t $ft██████$d$f7██$t $ft██████$d$f7██$t 
 
EOF

}

# Source Code Control
ga() { git add "$@"; }
gc() { git commit -m "$@"; }
parse_git_branch() {
	git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ ( \1)/'
}

