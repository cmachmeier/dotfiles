# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# history: Configurations
HISTCONTROL="ignoredups:ignorespace"
HISTFILESIZE=-1
HISTIGNORE="history*:ls *:gh"
HISTSIZE=-1
HISTTIMEFORMAT="%Y-%m-%d %H:%M:%S >: "

shopt -s histappend

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# Allows you to cd into a directory merely by typing the directory's name.
shopt -s autocd

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
export LESSHISTFILE=-

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

#
# PROMPT
#
# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
        # We have color support; assume it's compliant with Ecma-48
        # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
        # a case would tend to support setf rather than setaf.)
        color_prompt=yes
    else
    	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\][\u@\h]\[\033[00m\] \[\033[01;34m\]\w\[\033[00m\]$(parse_git_branch) >: '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

#
# COLOR SUPPORT
#
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

#
# ALIASES
#
# DEV
alias cr='~/.scripts/check_repos'
alias docker='sudo docker'
alias dotty='/usr/bin/git --git-dir=$HOME/.dotFiles/ --work-tree=$HOME'
alias dottyls='dotty ls-tree --full-tree --name-only HEAD'
alias dottyll='dotty ls-tree --full-tree -r --name-only HEAD'
alias gp='git push'
alias lint_changes="for i in $(git status | grep modified | grep php | awk '{print $2}'); do php -l $i; done"

# File operations
alias ..='cd ../'
alias ...='cd ../../'
alias gh='clear; cd ~;'
alias igrep='grep -i'
alias ls='ls -1CF --color=auto'
alias ll='ls -laGbh'
alias md='mkdir -p'
alias more='less'
alias open='xdg-open'
alias re='clear; cd ~/Repositories && ll'
alias sxiv='sxiv -r -t'

# FUN
alias ricebowl='curl -L rum.sh/ricebowl'

# Media
unset scf
alias as3='export scf="screenshot-$(date +'%Y%m%d%H%I%S').png"; scrot $scf; mv $scf $HOME/Pictures/'
alias as4='export scf="screenshot-$(date +'%Y%m%d%H%I%S').png"; scrot --line style=solid,width=1,color="red" --select $scf; mv $scf $HOME/Pictures/'
alias superdrive='sg_raw /dev/sr0 EA 00 00 00 00 00 01'

# Misc
alias df='df -h'
alias du='du -hs'
alias mc='mc --nosubshell'
alias nb='newsboat'

# Network
alias ipp='curl ipinfo.io/ip; echo ""'
alias transfer='~/.scripts/transfer.sh'

# System
alias ddq='xrandr --output HDMI-1 --mode 2560x1440 --primary'
alias ddt='~/.scripts/mirror_monitor'
alias lk='ssh-add -l'
alias mm='~/.scripts/mirror_monitor'
alias m2m='~/.scripts/monitor_manager.py'
alias reboot='sudo shutdown -r now'
alias sb='source ~/.bashrc'
alias sd='~/.scripts/lock_screen'
alias update_system='sudo apt-get update -y && \
				     sudo apt-get full-upgrade --allow-downgrades -y && \
				     sudo apt-get autoremove -y && \
				     sudo apt-get clean -y'
alias system_update='update_system'

# Themes
alias tgd='$HOME/.scripts/theme.sh gruvbox-dark'
alias tgl='$HOME/.scripts/theme.sh gruvbox'
alias tsd='$HOME/.scripts/theme.sh solarized-dark'
alias tsl='$HOME/.scripts/theme.sh solarized-light'

#
# FUNCTIONS
#
if [ -f $HOME/.bash_functions ]; then
    . $HOME/.bash_functions
fi

#
# BINDIRS
#
if [ -d "$HOME/.local/bin" ] ; then
    export PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/bin" ] ; then
    export PATH="$HOME/bin:$PATH"
fi

export PATH="/usr/local/php7/bin:$PATH"

#
# "PATHS"
#
export REPO_DIR="$HOME/Repositories"

#
# SSH
#
if [ ! -S ~/.ssh/ssh_auth_sock ]; then
  eval "$(ssh-agent)"
  ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
fi
export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock

ssh-add $HOME/.ssh/cm-git.privateKey
ssh-add $HOME/.ssh/cm.privateKey
# ssh-add $HOME/.ssh/funanga-cm-bitbucket.privateKey
# ssh-add $HOME/.ssh/funanga-ec2.privateKey

#
# MISCELLANEOUS
#
export BROWSER=firefox
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export EDITOR=vim
export VISUAL=$EDITOR
export PAGER=less

#
# CONFIGURATION: nnn
#
export NNN_BMS="D:$HOME/Documents/;d:$HOME/Downloads/;F:$HOME/Family/;f:$HOME/Finance/;K:$HOME/FunKitchenSync/;h:$HOME/Health/;k:$HOME/KitchenSync/;p:$HOME/Pictures/;v:$HOME/Videos/;w:$HOME/Work/"
export NNN_FIFO=/tmp/nnn.fifo
export NNN_PLUG='c:cppath;p:preview-tabbed;v:imgview;s:setbg;z:zip-it;'
#export NNN_FCOLORS='c1e2e52e006033f7c6d6abc4'

#
# "STARTUP"
#
# $HOME/.scripts/theme.sh nord
if [ $HOSTNAME = "qube" ]; then
	tsd
	ddq
elif [ $HOSTNAME = "tux" ]; then
	tgd
fi

figlet $HOSTNAME

