#!/usr/bin/env bash

#set -x

if [ $# -eq 0 ]; then
    echo "No arguments specified! Usage: \`transfer.sh /tmp/test.md\` or \`cat /tmp/test.md | transfer.sh test.md\`"
    exit 1
fi

# get temporarily filename, output is written to this file show progress can be showed
tmpfile=$( mktemp -t transferXXX )
file=$1

if [ "$( tty )" ]; then
    basefile=$(basename "$file" | sed -e 's/[^a-zA-Z0-9._-]/-/g')

    if [ ! -e $file ]; then
        echo "File $file doesn't exists."
        exit 1
    fi

    # transfer directory
    if [ -d $file ]; then
        zipfile=$( mktemp -t transferXXX.zip )
        cd $(dirname $file) && zip -r -q - $(basename $file) >> $zipfile
        curl --progress-bar -H "Max-Downloads: 1" -H "Max-Days: 1" -H "Deletion-URL: true" --upload-file "$zipfile" "https://transfer.sh/$basefile.zip" >> $tmpfile
        rm -f $zipfile

    # transfer file
    else
        curl --progress-bar -H "Max-Downloads: 1" -H "Max-Days: 1" -H "Deletion-URL: true" --upload-file "$file" "https://transfer.sh/$basefile" >> $tmpfile
    fi

# transfer pipe
else
    curl --progress-bar -H "Max-Downloads: 1" -H "Max-Days: 1" -H "Deletion-URL: true" --upload-file "-" "https://transfer.sh/$file" >> $tmpfile
fi

cat $tmpfile
rm -f $tmpfile
echo ""
