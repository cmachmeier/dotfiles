"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (has("nvim"))
  "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
  let $NVIM_TUI_ENABLE_TRUE_COLOR=1
endif
"For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
"Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
"< https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
if (has("termguicolors"))
  set termguicolors
endif

noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

set autoindent
set backspace=indent,eol,start
set cindent
set clipboard+=unnamedplus
set cursorline
set diffopt+=iwhite
set expandtab
set fileformat=unix
set hlsearch
set ignorecase
set incsearch
set linebreak
set nobackup
set nocompatible
set noexpandtab
set nopaste
set number
set ruler
set shiftwidth=4
set showbreak=+++
set showmatch
set showmode
set smartcase
set smartindent
set smarttab
set softtabstop=4
set tabstop=4
set textwidth=100
set title
set undolevels=1000
set visualbell

syntax on

"colorscheme onedark
"let g:airline_theme='onedark'
colorscheme wal
let g:airline_theme='wal'
